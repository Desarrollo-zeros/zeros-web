<?php

namespace App\Http\Controllers;

use App\Exceptions\WebException;
use App\Model\Web;
use App\Model\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WebController extends Controller
{
    /**
     * @param Request $request
     * @param Web $web
     * @param Auth $auth
     * @return \Illuminate\Http\JsonResponse
     */
    public function setDataAccount(Request $request, Web $web, Auth $auth){
       $validate = $auth->getValidateTable("account"); // obtener datos para validar
       $validator = Validator::make($request->all(),$validate); // obtengo el dato validado

       if($validator->fails()){
           return response()->json((string)$validator->errors());
       }

       $data1 = [
           "username"=> $request->username,
           "sha_pass_hash"=> $auth->getSha_pass_hash($request->username,$request->password),
           "email" => $request->email,
           "expansion" => 3
       ];

       $data2 = [
           "username"=> $request->username,
           "password"=> $web->getPassword($request->username,$request->password),
           "email" => $request->email,
           "rango" => 0,
           "imagen" => $web->imgPlayer[mt_rand(0,4)]
       ];

       if($auth->setAccount($data1)){
           if($web->setAccount($data2)){
              return response()->json($web->success("100"));
           }else{
               return response()->json($web->error("100"));
           }
       }
       return response()->json($auth->error("100"));
   }


    /**
     * @param Request $request
     * @param Web $web
     * @param Auth $auth
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDataAccount(Request $request, Web $web, Auth $auth){
       $validate = $web->getValidateTable("login"); // obtener datos para validar
       $validator = Validator::make($request->all(),$validate); // obtengo el dato validado

       if($validator->fails()){
           return response()->json((string)$validator->errors());
       }
       $account = $web->getAccount($request->username,$web->getPassword($request->username,$request->password));
       if(!$account){
           return response()->json($web->error("101"));
       }
       return response()->json([$web->success("101"),"account"=>$account]);
   }

}
