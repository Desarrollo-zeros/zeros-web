<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Auth extends Model
{
    protected $table = null;
    private $db = null;
    protected $connection =  "Auth";

    /**
     * Auth constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setConnection($this->connection);
        $this->db = DB::connection($this->connection);
    }

    /**
     * @param $username
     * @param $sha_pass_hash
     * @table account
     * @return array|bool
     */
    public function getAccount($username, $sha_pass_hash){
        $this->table = "account";
        $account = $this->db->table($this->table)
            ->where(compact("username"))
            ->where(compact("sha_pass_hash"))
            ->get($this->getDataTable($this->table))
            ->first();
        return isset($account) ? $account : false;
    }

    /**
     * @param string $nameTable
     * @return mixed
     */
    public function getDataTable($nameTable = ""){
        $data["account"] = [
            "id",
            "username",
            "email"
        ];
        return $data[$nameTable];
    }

    /**
     * @param array $data
     * @return bool
     */
    public function setAccount($data = []){
        if($this->fill($data)->save()){
            return true;
        }
        return false;
    }

    /**
     * @param string $nameTable
     * @return mixed
     */
    public function getValidateTable($nameTable = ""){
        $data["account"] = [
            "username"=>"required|string|max:20|min:4|unique:account",
            "sha_pass_hash" => "required|string|max:255|min:20|unique:account",
            "email" => "required|email|max:255|min:20|unique:account",
        ];
        return $data[$nameTable];
    }

    /**
     * @param $username
     * @param $sha_pass_hash
     * @return string
     */
    public function getSha_pass_hash($username, $sha_pass_hash){
        if (!is_string($username))
            $username = "";
        if (!is_string($sha_pass_hash))
            $sha_pass_hash = "";
        return  sha1(strtoupper($username).':'.strtoupper($sha_pass_hash));
    }


    public function error($nameError = ""){
        $error["100"] = [
            "error"=> 100,
            "mensaje"=> "No se pudo registrar, comunicate con un Maestro del juego o vuelve a intertarlo",
            "database" => "Auth",
            "table" => "account",
            "estado" => false
        ]; //error al registrar
        return $error[$nameError];
    }

    public function success($nameSuccess = ""){
        $success["100"] = [
            "success"=> 100,
            "mensaje"=> "Cuenta registrada con exito",
            "database" => "Auth",
            "table" => "account",
            "estado" => true
        ]; //cuenta registrada con exito
        return $success[$nameSuccess];
    }

}
